# Turnie.re Infrastructure as Code

## Preparing a server
1. Create a new server with Debian 12
2. Setup the server:
```bash
make initial-config.root
```
This will lock out root, therefore future runs should be done using `make initial-config`.

3. Deploy project files:
```bash
make deploy-applications
```
