INVENTORY_FILE = -i ansible/inventory
TURNIERE_ANSIBLE_VAULT_PASSWORD_FILE ?= ~/.turniere_ansible_vault_password_file
ANSIBLE_VAULT_PASSWORD_FILE_OPTION := --vault-password-file $(TURNIERE_ANSIBLE_VAULT_PASSWORD_FILE)

ANSIBLE_PLAYBOOK = ansible-playbook $(INVENTORY_FILE) $(ANSIBLE_VAULT_PASSWORD_FILE_OPTION)
deploy:
	$(ANSIBLE_PLAYBOOK) -u dschaedler ansible/deploy_applications.yml

initial-config.root:
	$(ANSIBLE_PLAYBOOK) -u root ansible/initial_config.yml

initial-config:
	$(ANSIBLE_PLAYBOOK) -u dschaedler ansible/initial_config.yml

dumptruck: dump_local_postgres_and_push_to_remote
dump_local_postgres_and_push_to_remote:
	$(ANSIBLE_PLAYBOOK) -u dschaedler ansible/dump_local_postgres_and_push_to_remote.yml

encrypt.%:
	ANSIBLE_VAULT_PASSWORD_FILE=$(TURNIERE_ANSIBLE_VAULT_PASSWORD_FILE) ansible-vault encrypt $(encrypt)

tunnel_to_db:
	ssh -L 5432:localhost:5432 dschaedler@ergebnisse.fritz.box
