import os
from os import environ
from pathlib import Path

from dotenv import set_key
from git import Repo

CONFIG_FILES = [
    Path("servers/host001.turnie.re/turniere/config.env"),
]


class CommitInformationMissingError(Exception):
    pass


class UpdateInformationMissingError(Exception):
    pass


try:
    GIT_USER_NAME = environ["GITLAB_USER_NAME"]
    GIT_USER_EMAIL = environ["GITLAB_USER_EMAIL"]
    GIT_SSH_KEY = environ["TURNIERE_AT_GITLAB_SSH_PRIVATE_KEY"]
    CI_COMMIT_BRANCH = environ["CI_COMMIT_BRANCH"]
    CI_PROJECT_PATH = environ["CI_PROJECT_PATH"]
except KeyError as key_error:
    raise CommitInformationMissingError from key_error

try:
    UPDATE_TARGET = environ["UPDATE_TARGET"]
    UPDATE_COMMIT_SHA = environ["UPDATE_VERSION"]
except KeyError as key_error:
    raise UpdateInformationMissingError from key_error


def update_config_files() -> None:
    """
    Updates the config files with the new version.
    """
    key = f"{UPDATE_TARGET}_CONTAINER_VERSION"

    # convert to uppercase
    key = key.upper()

    # convert everything that is not A-Z or 0-9 to _
    key = "".join([c if c.isalnum() and c.isascii() else "_" for c in key])

    for file in CONFIG_FILES:
        set_key(dotenv_path=file, key_to_set=key, value_to_set=UPDATE_COMMIT_SHA)

    # HACKY! remove all single quotes from all CONFIG_FILES
    for file in CONFIG_FILES:
        with open(file, "r") as f:
            lines = f.readlines()
        with open(file, "w") as f:
            for line in lines:
                f.write(line.replace("'", ""))

def push_updated_config_files_to_gitlab() -> None:
    """
    Pushes the current history file and history additions file contents to gitlab.
    """

    # Initialize the repository
    repo = Repo('.')
    repo.git.checkout(CI_COMMIT_BRANCH)

    # setup git
    repo.git.remote("set-url", "origin", f"git@gitlab.com:{CI_PROJECT_PATH}.git")

    repo.config_writer().set_value("user", "name", GIT_USER_NAME).release()
    repo.config_writer().set_value("user", "email", GIT_USER_EMAIL).release()

    update_config_files()

    for file in CONFIG_FILES:
        # Display git diff
        print(repo.git.diff(file))
        # add file to git commit
        repo.git.add(file)

    # create commit
    repo.index.commit(f"Update {UPDATE_TARGET} to {UPDATE_COMMIT_SHA} \n\n[ci skip]")

    # push to gitlab
    os.chmod(GIT_SSH_KEY, 0o600)
    git_ssh_command = f"ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -o IdentityFile={GIT_SSH_KEY}"
    with repo.git.custom_environment(GIT_SSH_COMMAND=git_ssh_command):
        repo.git.push()


if __name__ == "__main__":
    push_updated_config_files_to_gitlab()
